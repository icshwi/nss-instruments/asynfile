# EPICS module to access files from an IOC

Using this module an IOC can access the contents of a configured directory;

* can query the files in the directory matching a specified pattern
* can read the contents of files

The module uses streamdevice to set the pattern to match filenames against, to set the filename to read from, and to read from the file itself.
The protocol file includes protocols for

* setting the pattern
* getting filenames matching the pattern
* setting the filename to read from
* an example on how to write your own protocol to parse a file

The module needs to be configured with

```drvAsynFileConfigure(<portname>, <path_to_expose>, <max_filesize>)```

where

1. ```portname``` is the usual asyn port name like, 'filereader',
2. ```path_to_expose``` is the directory that is accessible to the IOC via this port, there is no default
3. ```max_filesize``` is the maximum filesize that can be read by the IOC, the default is zero