/*
 #
 #  Copyright (c) 2018 - Present  European Spallation Source ERIC
 #
 # StreamDevice format converter for EPICS string waveform record support
 #
 # 
 # Author  : Krisztian Loki
 # email   : krisztian.loki@esss.se
 # Date    : 
 # version : 
 # 
*/
#include <stdio.h>
#include "StreamError.h"
#include "StreamFormatConverter.h"

#define LOG_PREFIX "StringArrayConverter:\t"

class StringArrayConverter : public StreamFormatConverter
{
    friend class StreamFormatConverterRegistrar<StringArrayConverter>;
private:
    StringArrayConverter() : string_converter(NULL) {}
    StringArrayConverter(const StringArrayConverter&);
    StreamFormatConverter* string_converter;

public:
    void provides(const char* name, const char* provided);
    int  parse(const StreamFormat &fmt, StreamBuffer &into, const char*& source, bool scanFormat);
    bool printString(const StreamFormat &fmt, StreamBuffer &output, const char* value);
    int  scanString(const StreamFormat &fmt, const char* input, char* value, size_t maxlen);
};


int StringArrayConverter::parse(const StreamFormat &fmt, StreamBuffer &into, const char*& source, bool scanFormat)
{
    if (!scanFormat)
    {
        StreamError("Format conversion %%a is only allowed in input format!\n");
        return false;
    }

    if (source[0] != 's')
    {
        StreamError("Only string-arrays are supported!\n");
        return false;
    }

    ++source;

    return string_converter->parse(fmt, into, source, scanFormat);
}


bool StringArrayConverter::printString(const StreamFormat &fmt, StreamBuffer &output, const char* value)
{
    return string_converter->printString(fmt, output, value);
}


int StringArrayConverter::scanString(const StreamFormat &fmt, const char* input, char* value, size_t maxlen)
{
    int length = string_converter->scanString(fmt, input, value, maxlen);

    if (length >= 0 && input[length] == '\0')
        ++length;

    return length;
}


void StringArrayConverter::provides(const char* name, const char* provided)
{
    string_converter = find('s');

    if (string_converter == NULL)
    {
        StreamError(LOG_PREFIX "Cannot get format converter for %%%c conversion, _NOT_ registering ourselves!\n", 's');
        return;
    }

    if (find(provided[0]))
    {
        StreamError(LOG_PREFIX "Found format converter for %%%c conversion, overriding with our own!\n", provided[0]);
    }

    StreamFormatConverter::provides(name, provided);
}


RegisterConverter (StringArrayConverter, "a");
